var express = require("express");
var app = express();
app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");

var server = require("http").Server(app);
var io = require("socket.io")(server);
server.listen(3000);

var users = [];
io.on("connection", function(socket){
  socket.on("client-reg", function(data){
    if(users.indexOf(data) >=0){
      socket.emit("server-reg-failed");
    }else{
      users.push(data);
      socket.username = data;
      socket.emit("server-reg-success", data);
      io.sockets.emit('server-send-list-users', users);
    }
  });
});

app.get("/", function(req, res){
  res.render("trangchu");
});
