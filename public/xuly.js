var socket = io("http://localhost:3000");

socket.on("server-reg-failed", function(){
  alert("existed user name");
});

socket.on("server-reg-success", function(data){
  $("#currentUser").html(data);
  $("#loginForm").hide(2);
  $("#chatForm").show(1);
});

socket.on("server-send-list-users", function(users){
  $("#boxContent").html("");
  users.forEach(function(i){
    $("#boxContent").append("<div class='user'>"+ i + "</div>");
  });
});

$(document).ready(function(){
  $("#loginForm").show();
  $("#chatForm").hide();


  $("#btnregister").click(function() {
    socket.emit("client-reg", $('#txtusername').val());
  });
});
